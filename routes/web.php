<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => ['auth'] 
], function() {
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    Route::get('/servicos-de-emissao', 'ServicosController@index')->name('admin.servicos');

    Route::post('/despachantes/habilitar/{id}', 'DespachantesController@enable');
    Route::post('/despachantes/desabilitar/{id}', 'DespachantesController@disable');

    Route::resource('despachantes', 'DespachantesController');
    Route::resource('usuarios', 'UsuariosController');
});

Route::group(['prefix' => 'endpoints', 'namespace' => 'Api'], function() {
    Route::get('despachantes/{id}', 'Admin\DespachantesApiController@show')->name('api.despachantes.show');
});

/**
 * Rotas Administrativas do Cliente
 */
Route::group([
    'prefix' => 'cliente',
    'namespace' => 'Cliente',
    'middleware' => ['auth'] 
], function() {
    Route::get('/', 'PainelController@index')->name('clientes.painel');
    Route::post('/requisitar-servico', 'RequisitarServicosController@store')->name('servicos.store');
    Route::post('/consultar-debitos', 'ConsultarDebitosController@consultar')->name('cliente.consultar-debitos');
    Route::post('/anexar-arquivo', 'AnexarArquivosController@store')->name('cliente.anexar-arquivo.store');
    Route::get('/download/{arquivo}', 'AnexarArquivosController@download')->name('cliente.download-arquivo');
});

/**
 * Rotas Administrativas do Despachante
 */
Route::group([
    'prefix' => 'despachante',
    'namespace' => 'Despachante',
    'middleware' => ['auth', 'logout.despachante.sem.acesso'] 
], function() {
    Route::get('/', 'PainelController@index')->name('despachantes.painel');
    Route::post('/alterar-situacao-do-servico', 'ServicosController@alterarStatus')->name('despachantes.servico.alterar-status');
});

/**
 * Demais Rotas
 */
Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/painel', 'HomeController@painel')->name('redirecionar.painel');
Route::post('/consultar-debitos', 'HomeController@consultarDebitos')->name('consultar-debitos');
