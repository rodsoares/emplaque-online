<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * Detran Sc
 */
Route::get('consultar-debitos/{placa}/{renavam}', 'Api\ConsultaDebitosApiController@index');
Route::get('municipios/{uf}', 'Api\MunicipiosApiController@consultar');
Route::get('municipios/{codigo}/despachantes', 'Api\MunicipiosApiController@despachantes');
