let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
    'resources/assets/js/admin/dashboard.js',
    'resources/assets/js/admin/despachantes.js',
    'resources/assets/js/admin/usuarios.js',
    'resources/assets/js/cliente/painel.js',
    'resources/assets/js/despachante/painel.js'
], 'public/js/app.js')
    .sass('resources/assets/sass/app.scss', 'public/css');
