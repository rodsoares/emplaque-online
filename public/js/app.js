$(document).ready(function() {
    var $tabela_ultimas_consultas =  $('#tabela-ultimas-consultas').DataTable({
         "language": {
             "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
         }
    });
});
$(document).ready(function() {
    var tabela_despachantes =  $('#tabela-despachantes').DataTable({
         "language": {
             "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
         }
    });
});

function exibirDespachante( id ) {
    var modal = $('#modal-exibir-despachante');
    var btn_exibir = $('#btn-exibir-despachante-'+id);

    btn_exibir.button('loading');

    /** obtendo dados */
    $.get('/endpoints/despachantes/' + id)
     .done(function( response ){
        var title = $('#modal-exibir-despachante #modal-exibir-despachante-label');
        var body  = $('#modal-exibir-despachante .modal-body');

        /** primeira linha */
        var html = '<div class="row">';

        html += '<div class="col-sm-12 col-md-5 col-lg-5">';
        html += '<a href="#" class="thumbnail">';
        if ( response.logo != null ) {
            html += '<img src="/uploads/'+ response.logo +'" class="img-responsive" alt="...">';
        } else {
            html += '<img src="http://placehold.it/200x110" class="img-responsive" alt="...">';
        }
        html += '</a>';
        html += '</div>';

        html += '<div class="col-sm-12 col-md-7 col-lg-7">';
        html += '<dl>';
        html += '<dt>Nome:</dt>';
        html += '<dd>'+ response.nome +'</dd>';
        html += '<dt>Email:</dt>';
        html += '<dd>'+ response.email +'</dd>';
        html += '<div class="row">';
        html += '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">';
        html += '<dt>Telefone:</dt>';
        html += '<dd>'+ response.telefone +'</dd>';
        html += '</div>';
        html += '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">';
        html += '<dt>Celular:</dt>';
        html += '<dd>'+ response.celular +'</dd>';
        html += '</div>';
        html += '</div>';
        html += '</dl>';
        html += '</div>';

        html += '</div>';
        
        /** Segunda linha */
        html += '<div class="row">';

        html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
        html += '<div class="box box-solid">';
        html += '<div class="box-body"><table class="table table-condensed table-bordered table-striped"><thead><tr class="bg-info"><th colspan="3" class="text-center">Serviços em Aberto</th></tr><tr><th>Cliente</th><th class="text-center">Serviço</th><th class="text-right">Status</th></tr></tr></thead><tbody><tr></tr></tbody></table></div></div>';
        html += '</div>';
        html += '</div>';

        html += '</div>';
        
        title.html('Despachante ' + response.nome);
        body.html(html);

        modal.modal('show');
     });

    modal.on('hidden.bs.modal', function () {
        btn_exibir.button('reset');
    });
}

function deletarDespachante( id ) {
    swal({
        title: "Você tem certeza ?",
        text: "Uma vez excluído você não será capaz de recuperá-lo!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            var form = $('#form-deletar-despachante');
            var url = location.href + '/' + id;
        
            document.querySelector('#form-deletar-despachante').reset(); 
            $('#form-deletar-despachante').attr('action', url);
            $('#form-deletar-despachante').submit();
        }
    });
}

function habilitarDespachante( id ) {
    var form = $('#form-deletar-despachante');
    var url = location.href + '/habilitar/' + id;
        
    document.querySelector('#form-deletar-despachante').reset(); 

    $('#form-deletar-despachante').attr('action', url);
    $('#form-deletar-despachante input[name="_method"]').val('POST');
    $('#form-deletar-despachante').submit();
}

function desabilitarDespachante( id ) {
    var form = $('#form-deletar-despachante');
    var url = location.href + '/desabilitar/' + id;
        
    document.querySelector('#form-deletar-despachante').reset(); 

    $('#form-deletar-despachante').attr('action', url);
    $('#form-deletar-despachante input[name="_method"]').val('POST');
    $('#form-deletar-despachante').submit();
}
$(document).ready(function() {
    var tabela_usuarios =  $('#tabela-usuarios').DataTable({
         "language": {
             "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
         }
    });
});

function exibirUsuario( id ) {
    var modal = $('#modal-exibir-usuario');
    var btn_exibir = $('#btn-exibir-usuario-'+id);

    btn_exibir.button('loading');

    /** obtendo dados */
    $.get('/endpoints/usuarios/' + id)
     .done(function( response ){
        var title = $('#modal-exibir-usuario #modal-exibir-usuario-label');
        var body  = $('#modal-exibir-usuario .modal-body');

        /** primeira linha */
        var html = '<div class="row">';

        html += '<div class="col-sm-12 col-md-5 col-lg-5">';
        html += '<a href="#" class="thumbnail">';
        html += '<img src="/storage/'+ response.logo +'" alt="...">';
        html += '</a>';
        html += '</div>';

        html += '<div class="col-sm-12 col-md-7 col-lg-7">';
        html += '<dl>';
        html += '<dt>Nome:</dt>';
        html += '<dd>'+ response.nome +'</dd>';
        html += '<dt>Email:</dt>';
        html += '<dd>'+ response.email +'</dd>';
        html += '<div class="row">';
        html += '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">';
        html += '<dt>Telefone:</dt>';
        html += '<dd>'+ response.telefone +'</dd>';
        html += '</div>';
        html += '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">';
        html += '<dt>Celular:</dt>';
        html += '<dd>'+ response.celular +'</dd>';
        html += '</div>';
        html += '</div>';
        html += '</dl>';
        html += '</div>';

        html += '</div>';
        
        /** Segunda linha */
        html += '<div class="row">';

        html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
        html += '<div class="box box-solid">';
        html += '<div class="box-body"><table class="table table-condensed table-bordered table-striped"><thead><tr class="bg-info"><th colspan="3" class="text-center">Serviços em Aberto</th></tr><tr><th>Cliente</th><th class="text-center">Serviço</th><th class="text-right">Status</th></tr></tr></thead><tbody><tr></tr></tbody></table></div></div>';
        html += '</div>';
        html += '</div>';

        html += '</div>';
        
        title.html('usuario ' + response.nome);
        body.html(html);

        modal.modal('show');
     });

    modal.on('hidden.bs.modal', function () {
        btn_exibir.button('reset');
    });
}

function deletarUsuario( id ) {
    swal({
        title: "Você tem certeza ?",
        text: "Uma vez excluído você não será capaz de recuperá-lo!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            var form = $('#form-deletar-usuario');
            var url = location.href + '/' + id;
        
            document.querySelector('#form-deletar-usuario').reset(); 
            $('#form-deletar-usuario').attr('action', url);
            $('#form-deletar-usuario').submit();
        }
    });
}
$('#modal-requisitar-servico').on('shown.bs.modal', function () {

    /**
     * Obtendo valores do servidor e populando select
     */
    var municipio = $('#form-requisitar-servico #municipio');

    $.get('/api/municipios/SC?to-select2')
     .done( function(response) {
        municipio.html(response)
     })

    municipio.select2();

    /**
     * alterando valores do select sempre um novo município for selecionado
     */
    municipio.change( function (event) {
        $.get('/api/municipios/'+ $(this).val() + '/despachantes/?to-select2')
         .done(function(response){
             var despachante = $('#form-requisitar-servico #despachante')
            
             despachante.html(response)
             despachante.attr('readonly', 'false') 
             despachante.select2();
         })
         .fail(function(){
            var despachante = $('#form-requisitar-servico #despachante')
            despachante.attr('readonly', 'false')
            despachante.html('')
            despachante.select2();
         })
    })
})

$('#modal-requisitar-servico').on('hidden.bs.modal', function () {
    var div = $('#div_endereco');
    if ( !div.hasClass('hide') )
        div.addClass('hide');
});

$('input[name="a_domicilio"]').change(function(event){
    var div = $('#div_endereco');
    if ( div.hasClass('hide') )
    {
        div.removeClass('hide');
    } else {
        div.addClass('hide');
    }
});

/**
 * Funções gerais
 */

function anexarArquivo( servico_id ) {
    var modal = $("#modal-anexar-arquivo");  
    // zerando valores anteriores
    document.querySelector('#form-anexar-arquivo').reset(); 
    // atribuindo id do serviço em questão
    $("#modal-anexar-arquivo #form-anexar-arquivo input[name='servico_id']").attr('value', +servico_id);  
    // exibindo o dito cujo
    modal.modal('show');
} 
function abrirDetalhes(id_servico = null) {
    
    if ( id_servico )
    {
        var linha = $("#detalhes-servico-"+id_servico);
        var btn = $("#btn-abrir-detalhes-"+id_servico);


        if ( linha.hasClass('hidden') ){
            linha.removeClass('hidden');
            btn.html('<i class="fa fa-fw fa-minus"></i>')
        } else {
            linha.addClass('hidden');
            btn.html('<i class="fa fa-fw fa-plus"></i>')
        }
    } else {
        /** obtendo dos ids dos servicos */
        $('td[name="id"]').each(function (index, value) {
            var id_servico = +($( value ).html());

            var linha = $("#detalhes-servico-"+id_servico);
            var btn = $("#btn-abrir-detalhes-"+id_servico);


            if ( linha.hasClass('hidden') ){
                linha.removeClass('hidden');
                btn.html('<i class="fa fa-fw fa-minus"></i>')
            } else {
                linha.addClass('hidden');
                btn.html('<i class="fa fa-fw fa-plus"></i>')
            }
        });
    } 
}

function abrirModalAlterarStatus( servico_id ) {
    var modal = $("#modal-alterar-status");  
    // zerando valores anteriores
    document.querySelector('#form-alterar-status').reset(); 
    // atribuindo id do serviço em questão
    $("#modal-alterar-status #form-alterar-status input[name='servico_id']").attr('value', +servico_id);  
    // exibindo o dito cujo
    modal.modal('show');
}