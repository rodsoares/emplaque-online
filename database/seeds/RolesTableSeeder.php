<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'adicionar usuarios',
            'adicionar despachantes',
            'editar usuarios',
            'editar despachantes',
            'deletar usuarios',
            'deletar despachantes',
            'visualizar usuarios',
            'visualizar despachantes',
            'visualizar servicos'
        ];

        $roles = [
            'administrador',
            'funcionario',
            'despachante',
            'cliente',
        ];

        foreach ( $permissions as $permission ) 
            Permission::create(['name' => $permission]);

        foreach ( $roles as $role ) 
            Role::create(['name' => $role]);

        $admin = Role::findByName('administrador');
        $cliente = Role::findByName('cliente');
        $despachante = Role::findByName('despachante');

        $admin->givePermissionTo([
            'adicionar usuarios',
            'adicionar despachantes',
            'editar usuarios',
            'editar despachantes',
            'deletar usuarios',
            'deletar despachantes',
            'visualizar usuarios',
            'visualizar despachantes',
            'visualizar servicos'
        ]);
    }
}
