<?php

use Illuminate\Database\Seeder;
use Emplaque\Models\User as Usuario;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Rodrigo Soares Souza',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin@admin.com')
        ]);

        DB::table('users')->insert([
            'name' => 'Cliente Teste',
            'email' => 'cliente@teste.com',
            'password' => bcrypt('cliente@teste.com')
        ]);
        
        $usuario = Usuario::first();
        $usuario->assignRole('administrador');
        $usuario->save();

        $usuario = Usuario::orderBy('id', 'DESC')->first();
        $usuario->assignRole('cliente');
        $usuario->save();
    }
}
