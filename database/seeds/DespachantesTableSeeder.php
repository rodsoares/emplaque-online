<?php

use Illuminate\Database\Seeder;

class DespachantesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Emplaque\Models\Despachante::class, 11)->create();
    }
}
