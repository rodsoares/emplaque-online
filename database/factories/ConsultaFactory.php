<?php

use Faker\Generator as Faker;

$factory->define(\Emplaque\Models\Consulta::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('pt_BR');
    
    $placa = '';
    for($i = 0; $i < 3; $i++)
        $placa .= $faker->randomLetter;

    $placa = mb_strtoupper($placa, 'UTF-8');
    $placa .= $faker->randomNumber($nbDigits = 4, true);
    $renavam = $faker->randomNumber($nbDigits = 9, true); 

    return [
        'nome' => $faker->name,
        'placa' => $placa,
        'renavam' => $renavam,
        'telefone' => $faker->unique()->cellphoneNumber,
        'email' => $faker->unique()->email,
    ];
});
