<?php

use Faker\Generator as Faker;

$factory->define(\Emplaque\Models\Despachante::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('pt_BR');
    return [
        'nome' => $faker->unique()->company,
        'telefone' => $faker->unique()->phoneNumber,
        'celular' => $faker->unique()->cellphoneNumber,
        'email' => $faker->unique()->safeEmail()
    ];
});
