<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateServicosTable.
 */
class CreateServicosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$status = [
			'AGUARDANDO PROCESSAMENTO',
			'AGUARDANDO COPIA DE DOCUMENTOS',
			'APROVADO',
			'INICIADO',
			'FINALIZADO',
			'NEGADO'
		];

		Schema::create('servicos', function(Blueprint $table) use ($status){
            $table->increments('id');
			$table->integer('user_id')->unsigned(); // belongsTo
			$table->integer('despachante_id')->unsigned(); // belongsTo
			$table->string('placa');
			$table->string('renavam');
			$table->string('email');
			$table->string('tel_contato');
			$table->boolean('a_domicilio')->default(false);
			$table->text('endereco')->nullable();
			$table->enum('status', $status)->default('AGUARDANDO PROCESSAMENTO');
			$table->timestamps();
			
			$table->foreign('user_id')
				  ->references('id')->on('users');
			
			$table->foreign('despachante_id')
				  ->references('id')->on('despachantes');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('servicos', function(Blueprint $table){
			$table->dropForeign(['user_id']);
			$table->dropForeign(['despachante_id']);
		});
		Schema::drop('servicos');
	}
}
