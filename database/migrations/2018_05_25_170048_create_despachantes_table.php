<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDespachantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('despachantes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('municipio_codigo')->unsigned()->nullable();
            $table->string('nome');
            $table->string('logo')->nullable();
            $table->string('telefone')->nullable();
            $table->string('celular')->nullable();
            $table->string('email')->nullable();
            $table->string('endereco')->nullable();
            $table->enum('status', [
                'AGUARDANDO APROVACAO', 
                'APROVADO', 
                'NEGADO'
            ])->default('AGUARDANDO APROVACAO');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('despachantes', function(Blueprint $table){
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('despachantes');
    }
}
