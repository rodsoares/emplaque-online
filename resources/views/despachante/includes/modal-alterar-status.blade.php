<!-- Modal -->
<div class="modal fade" id="modal-alterar-status" role="dialog" aria-labelledby="modal-alterar-status-label">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-alterar-status-label"><i class="fa fa-fw fa-upload"></i> Alterar Situação do Serviço</h4>
                </div>
                <form 
                    id="form-alterar-status" 
                    action="{{ route('despachantes.servico.alterar-status') }}" 
                    method="POST"
                >
                    {{ csrf_field() }}
                    <input type="hidden" name="servico_id" />
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="nome">Situação do serviço</label>
                                    <select id="municipio" class="form-control" name="status" width="100%" required>
                                        @php 
                                            $status = [
                                                'AGUARDANDO PROCESSAMENTO',
                                                'AGUARDANDO COPIA DE DOCUMENTOS',
                                                'APROVADO',
                                                'INICIADO',
                                                'NEGADO'
                                            ];
                                        @endphp
                                            <option></option>
                                        @foreach( $status as $item )
                                            <option value="{{ $item }}">{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <i class="fa fa-fw fa-close"></i>
                            Fechar
                        </button>
                        <button type="submit" class="btn btn-primary">Alterar Situação</button>
                    </div>
                </form>
            </div>
        </div>
    </div>