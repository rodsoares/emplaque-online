@extends('adminlte::page') 

@section('content')
<div class="box box-solid">
    <div class="box-header">
        <h3 class="box-title">Serviços de emissão de documento requisitados</h3>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-hovered">
                <thead>
                    <tr>
                        <th class="hidden"></th>
                        <th>Status</th>
                        <th>Placa</th>
                        <th>Renavam</th>
                        <th>Serviço</th>
                        <th>Cliente</th>
                        <th></th>
                        <th>
                            <button id="btn-abrir-detalhes" 
                                    class="btn btn-default btn-xs" 
                                    title="abrir/fechar detalhes"
                                    onclick="abrirDetalhes()">
                                <i class="fa fa-fw fa-align-justify"></i>
                            </button>
                        </th>
                    </tr>
                </thead>

                <tbody>
                    @foreach( $servicos as $servico )
                        <tr>
                            <td name="id" class="hidden">{{ $servico->id }}</td>
                            <td>
                                @if ( $servico->status === 'AGUARDANDO PROCESSAMENTO')
                                    <span class="label label-default">{{ $servico->status }}</span>
                                @elseif( $servico->status === 'NEGADO' )
                                    <span class="label label-danger">{{ $servico->status }}</span>
                                @elseif( $servico->status === 'FINALIZADO' )
                                    <span class="label label-success">{{ $servico->status }}</span>
                                @else
                                    <span class="label label-primary">{{ $servico->status }}</span>
                                @endif
                            </td>
                            <td>{{ $servico->placa }}</td>
                            <td>{{ $servico->renavam }}</td>
                            <td> 
                                Emissão de documento &nbsp;
                                @if ( $servico->a_domicilio )
                                    <span class="label label-success">entrega em domicílio</span>
                                @else
                                    <span class="label label-default">retirar no despachante</span>
                                @endif
                            </td>
                            <td>{{ $servico->cliente->name }}</td>
                            <td> 
                                <button 
                                    class="btn btn-xs btn-block btn-default"
                                    onclick="javascript:abrirModalAlterarStatus({{ $servico->id }})">
                                    Alterar Situação
                                </button>
                            </td>
                            <td> 
                                <button id="btn-abrir-detalhes-{{ $servico->id }}" 
                                        class="btn btn-default btn-xs"
                                        onclick="abrirDetalhes({{ $servico->id }})" >
                                    <i class="fa fa-fw fa-plus"></i>
                                </button> 
                            </td>
                        </tr>
                        
                        <tr id="detalhes-servico-{{ $servico->id }}" class="hidden">
                            <td colspan="5">
                                <h5><b>INFORMAÇÕES REFERENTES AO PEDIDO</b></h5>
                                <b>Telefone para contato:</b> {{ $servico->tel_contato }} <br />
                                <b>Email:</b> {{ $servico->email }}<br/>
                                <b>Data do pedido:</b> {{ $servico->created_at->format('d-m-Y') }} às {{ $servico->created_at->format('H:i') }}
                                 @if( $servico->a_domicilio)
                                    <br/>
                                    <b>Endereço:</b> {{ $servico->endereco }} 
                                @endif

                                <hr />

                                <h5>
                                    <b>DOCUMENTOS ANEXADOS</b> 
                                    <button 
                                        class="btn btn-default btn-xs" 
                                        onclick='javascript:document.querySelector("#form-anexar-arquivo").reset();anexarArquivo( {{ $servico->id }} )'
                                    >
                                        <i class="fa fa-fw fa-plus"></i>
                                        anexar
                                    </button>
                                </h5>

                                @if ( count($servico->arquivos) )
                                    @foreach( $servico->arquivos->sortByDesc('id')->all() as $documento )
                                        <b>{{ $documento->created_at->format('d-m-Y H:i') }}</b> 
                                        <a 
                                            href="{{ route('cliente.download-arquivo', ['id' => $documento->id ]) }}" 
                                            target="_blank"
                                            class="label label-info"
                                        >
                                            {{ $documento->nome }}
                                        </a> <br />
                                    @endforeach
                                @else
                                    Não há documentos anexados ao serviço
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@include('cliente.includes.modal-anexar-arquivo')
@include('despachante.includes.modal-alterar-status')
@stop