@extends('adminlte::page')

@section('content')
    <div class="box box-solid">
        <div class="box-body">
            <dl>
                <div class="row">
                    <div class="col-sm-6 col-md-2 col-lg-2">
                        <dt>Placa</dt>
                        <dd><?= $info[0][0]?></dd>
                    </div>
                    <div class="col-sm-6 col-md-2 col-lg-2">
                        <dt>Renavam</dt>
                        <dd><?= $info[0][1]?></dd>
                    </div>
                    <div class="col-sm-6 col-md-2 col-lg-2">
                        <dt>Placa Anterior</dt>
                        <dd><?= $info[0][2]?></dd>
                    </div>
                    <div class="col-sm-6 col-md-2 col-lg-2">
                        <dt>Tipo</dt>
                        <dd><?= $info[0][3]?></dd>
                    </div>

                     <div class="col-sm-6 col-md-2 col-lg-2">
                        <dt>Categoria</dt>
                        <dd><?= $info[0][4]?></dd>
                    </div>

                    <div class="col-sm-6 col-md-2 col-lg-2">
                        <dt>Espécie</dt>
                        <dd><?= $info[0][5]?></dd>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6 col-md-2 col-lg-2">
                        <dt>Lugares</dt>
                        <dd><?= $info[0][6]?></dd>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <dt>Marca/Modelo</dt>
                        <dd><?= $info[1][0]?></dd>
                    </div>
                    <div class="col-sm-6 col-md-2 col-lg-2">
                        <dt>Fabricação/Modelo</dt>
                        <dd><?= $info[1][1]?></dd>
                    </div>
                    <div class="col-sm-6 col-md-2 col-lg-2">
                        <dt>Combustível</dt>
                        <dd><?= $info[1][2]?></dd>
                    </div>
                    <div class="col-sm-6 col-md-2 col-lg-2">
                        <dt>Cor</dt>
                        <dd><?= $info[1][3]?></dd>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-3 col-lg-3">
                        <dt>Carroceria</dt>
                        <dd><?= $info[1][4]?></dd>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-3">
                        <dt>Categoria DPVAT</dt>
                        <dd><?= $info[1][5]?></dd>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <dt>Nome do Proprietário Atual</dt>
                        <dd><?= $info[2][0]?></dd>
                    </div>
                    <div class="col-sm-6 col-md-2 col-lg-2">
                        <dt>Recadastrado DETRAN</dt>
                        <dd><?= $info[2][1]?></dd>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <dt>Nome do Proprietário Anterior</dt>
                        <dd><?= $info[3][0]?></dd>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-3">
                        <dt>Origem dos dados do Veículo</dt>
                        <dd>
                            <b class="text-danger">
                                <?= $info[3][1]?>
                            </b>
                        </dd>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-3">
                        <dt>Município de Emplacamento</dt>
                        <dd><?= $info[4][0]?></dd>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <dt>Licenciado</dt>
                        <dd><?= $info[4][1]?></dd>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-3">
                        <dt>Data de Aquisição</dt>
                        <dd><?= $info[4][2]?></dd>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-3">
                        <dt>Situação</dt>
                        <dd>
                            <b class="text-danger">
                                <?= $info[4][3]?>
                            </b>
                        </dd>
                    </div>
                </div>
            </dl>
        </div>
    </div>

    <br />
    
    <div class="box box-solid">
        <div class="box-body">
            <dl>
                <div class="row">
                    <div class="col-sm-12 col-sm-4 col-md-4 col-lg-4">
                        <dt>Restrição à venda</dt>
                        <dd><?= $info[5][0]?></dd>
                    </div>
                    <div class="col-sm-12 col-sm-4 col-md-4 col-lg-4">
                        <dt>Informações pendentes (SNG)</dt>
                        <dd><?= $info[6][0]?></dd>
                    </div>
                    <div class="col-sm-12 col-sm-4 col-md-4 col-lg-4">
                        <dt>Restrições</dt>
                        <dd><?= $info[7][0]?></dd>
                    </div>
                </div>
            </dl>
        </div>
    </div>

    <br />

    <div class="box box-solid">
        <div class="box-body">
            <div class="table-responsive">
                <?php if( count( $debitos['detalhado'][0]) > 1 ): ?>
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="7">Listagem de Débitos</th>
                            </tr>
                            <tr>
                                <th>Classe</th>
                                <th>Nº DetranNET</th>
                                <th>Vencimento</th>
                                <th>Valor Nominal (R$)</th>
                                <th>Multa (R$)</th>
                                <th>Juros (R$)</th>
                                <th>Valor Atual (R$)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach( $debitos['detalhado'] as $index => $debito ): ?>
                                <?php if ( ($index != 4) && ($index != 0) ): ?>
                                    <tr>
                                        <td><?= $debito[0] ?></td>
                                        <td><?= $debito[1] ?></td>
                                        <td><?= $debito[2] ?></td>
                                        <td class="text-right"><?= $debito[3] ?></td>
                                        <td class="text-right"><?= $debito[4] ?></td>
                                        <td class="text-right"><?= $debito[5] ?></td>
                                        <td class="text-right"><?= $debito[6] ?></td>
                                    </tr>
                                <?php endif?>
                            <?php endforeach ?>
                        </tbody>
                        <tfooter>
                            <tr>
                                <th colspan="7" class="text-right">
                                    Total dos débitos: &nbsp;<i><?= $debitos['detalhado'][4][1] ?></i>
                                </th>
                            </tr>
                        </tfooter>
                    </table>
                <?php else: ?>
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="7">Listagem de Débitos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="table-warning text-center">
                                <td colspan="7"><?= $debitos['detalhado'][0][0] ?></td>
                            </tr>
                        </tbody>
                        <tfooter>
                            <tr>
                                <th colspan="7" class="text-right">
                                    Total dos débitos: &nbsp;<i>R$ 0,00</i>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="7" class="text-right">
                                    <h5 class="text-uppercase">Deseja emitir seu documento com um de nossos despachantes ?</h5> 
                                    <a class="btn btn-default" href="/">Não, obrigado (a)</a>
                                    <button 
                                        class="btn btn-primary"
                                        onclick="javascript:document.querySelector('#form-requisitar-servico').reset();$('#modal-requisitar-servico').modal('show')"
                                    >
                                        Sim, desejo
                                    </button>
                                </th>
                            </tr>
                        </tfooter>
                    </table>
                <?php endif ?>
            </div>
        </div>
    </div>

    @include('cliente.includes.modal-requisitar-servicos')
@stop