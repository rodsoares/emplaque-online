@extends('adminlte::page') 

@section('content')
<div class="box box-solid">
    <div class="box-header">
        <h3 class="box-title">Consulta Rápida de Débitos <small>Detran de Santa Catarina</small></h3>
    </div>
    <div class="box-body">
        <form id="consultar-debitos-form" action="{{ route('cliente.consultar-debitos') }}" method="POST">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-3">           
                    <div class="form-group">
                        <label for="placa">Placa</label>
                        <input type="text" class="form-control" id="placa" placeholder="Placa do veículo" name="placa" required>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="renavam">Renavam</label>
                        <input type="text" class="form-control" id="renavam" placeholder="Renavam do veículo" name="renavam" required>
                    </div>
                </div>

                <div class="col-sm-2">
                    <div style="margin-top: 25px;">
                        <button type="submit" class="btn btn-default btn-block">
                            <i class="fa fa-fw fa-search"></i> 
                            Consultar Débitos
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="box box-solid">
    <div class="box-header">
        <h3 class="box-title">Requisições realizadas</h3>
        <div class="clearfix text-right" style="float:right">
            <button class="btn btn-primary" 
                    onclick="javascript:document.querySelector('#form-requisitar-servico').reset();$('#modal-requisitar-servico').modal('show')"
            >
                <i class="fa fa-fw fa-plus"></i>
                Requisitar Documento CRLV
            </button>
        </div>
    </div>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-hovered">
                <thead>
                    <tr>
                        <th class="hidden"></th>
                        <th>Status</th>
                        <th>Placa</th>
                        <th>Renavam</th>
                        <th>Serviço</th>
                        <th>Despachante</th>
                        <th class="text-center">Atualização</th>
                        <th>
                            <button id="btn-abrir-detalhes" 
                                class="btn btn-default btn-xs" 
                                title="abrir/fechar detalhes"
                                onclick="abrirDetalhes()">
                                <i class="fa fa-fw fa-align-justify"></i>
                            </button>
                        </th>
                    </tr>
                </thead>

                <tbody>
                    @foreach( $servicos->sortByDesc('id')->all() as $servico )
                        <tr>
                            <td name="id" class="hidden">{{ $servico->id }}</td>
                            <td>
                                @if ( $servico->status === 'AGUARDANDO PROCESSAMENTO')
                                    <span class="label label-default">{{ $servico->status }}</span>
                                @elseif( $servico->status === 'NEGADO' )
                                    <span class="label label-danger">{{ $servico->status }}</span>
                                @elseif( $servico->status === 'FINALIZADO' )
                                    <span class="label label-success">{{ $servico->status }}</span>
                                @else
                                    <span class="label label-primary">{{ $servico->status }}</span>
                                @endif
                            </td>
                            <td>{{ $servico->placa }}</td>
                            <td>{{ $servico->renavam }}</td>
                            <td> 
                                Emissão de documento &nbsp;
                                @if ( $servico->a_domicilio )
                                    <span class="label label-success">entrega em domicílio</span>
                                @else
                                    <span class="label label-default">retirar no despachante</span>
                                @endif
                            </td>
                            <td>{{ $servico->despachante->nome }} <i>({{ $servico->despachante->email }})</i></td>
                            <td class="text-center">{{ $servico->updated_at->format('d-m-Y') }} às {{ $servico->updated_at->format('H:i') }}</td>
                            <td> 
                                <button id="btn-abrir-detalhes-{{ $servico->id }}" 
                                    class="btn btn-default btn-xs"
                                    onclick="abrirDetalhes({{ $servico->id }})" >
                                    <i class="fa fa-fw fa-plus"></i>
                                </button> 
                            </td>
                        </tr>
                        <tr id="detalhes-servico-{{ $servico->id }}" class="hidden">
                            <td colspan="6">
                                <h5><b>DADOS DO ADICIONAIS</b></h5>
                                <b>Cliente:</b> {{ $servico->cliente->name }} <br />
                                <b>Tel. Contato:</b> {{ $servico->tel_contato }}
                                @if ( $servico->a_domicilio )
                                    <br />
                                    <b>Local para entrega:</b> {{ $servico->endereco }}
                                @endif
                                <hr />

                                <h5><b>DADOS DO DESPACHANTE</b></h5>
                                <b>Nome:</b> {{ $servico->despachante->nome }} <br />
                                <b>Municipio:</b> {{ $servico->despachante->municipio->Nome }} <br />
                                <b>Endereço:</b> {{ $servico->despachante->endereco}} <br />
                                <b>Contato:</b> {{ $servico->despachante->telefone}} {{ $servico->despachante->celular }} <br />
                                <b>Email:</b> {{ $servico->despachante->email }}

                                <hr />

                                <h5>
                                    <b>DOCUMENTOS ANEXADOS</b> 
                                    <button 
                                        class="btn btn-default btn-xs" 
                                        onclick='javascript:document.querySelector("#form-anexar-arquivo").reset();anexarArquivo( {{ $servico->id }} )'
                                    >
                                        <i class="fa fa-fw fa-plus"></i>
                                        anexar
                                    </button>
                                </h5>

                                @if ( count($servico->arquivos) )
                                    @foreach( $servico->arquivos->sortByDesc('id')->all() as $documento )
                                        <b>{{ $documento->created_at->format('d-m-Y H:i') }}</b> 
                                        <a 
                                            href="{{ route('cliente.download-arquivo', ['id' => $documento->id ]) }}" 
                                            target="_blank"
                                            class="label label-info"
                                        >
                                            {{ $documento->nome }}
                                        </a> <br />
                                    @endforeach
                                @else
                                    Não há documentos anexados ao serviço
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@include('cliente.includes.modal-requisitar-servicos')
@include('cliente.includes.modal-anexar-arquivo')
@stop

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/busy-load/dist/app.min.js"></script>
    <script>
        $('#consultar-debitos-form').submit(function(event){
            // with options
            $.busyLoadFull("show", {
                color: "black", 
                spinner: "cube-grid",
                text: "Carregando informações ...",
                fontSize: "2rem",
                animation: "slide"
            });
        });
    </script>
@endsection