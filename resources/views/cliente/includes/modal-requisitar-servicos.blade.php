<!-- Modal -->
<div class="modal fade" id="modal-requisitar-servico" role="dialog" aria-labelledby="modal-requisitar-servico-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-requisitar-servico-label">Requisitar Emissão de Documento</h4>
            </div>
            <form id="form-requisitar-servico" action="{{ route('servicos.store') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" />
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="placa">Placa</label>
                                <input type="text" class="form-control" id="placa" placeholder="Placa do veículo" name="placa" value="{{ Session::get('cliente')['placa'] }}" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="renavam">Renavam</label>
                                <input type="text" class="form-control" id="renavam" placeholder="Renavam do veículo" name="renavam" value="{{ Session::get('cliente')['renavam'] }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="tel_contato">Telefone para contato</label>
                                <input type="text" 
                                    class="form-control" 
                                    id="tel_contato" 
                                    placeholder="(00) 00000-00000" 
                                    data-mask="(00) 00000-00000" 
                                    name="tel_contato" 
                                    value="{{ Session::get('cliente')['telefone'] }}"
                                    required
                                >
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" placeholder="seu@email.com.br" name="email" value="{{ Session::get('cliente')['email'] }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label for="municipio">Municipio</label>
                                <select id="municipio" class="form-control" name="municipio" width="100%" required>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-7">
                            <div class="form-group">
                                <label for="despachante">Despachante</label>
                                <select id="despachante" class="form-control" name="despachante_id" required>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="a_domicilio" value="1"> Deseja receber o documento em sua casa ?
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group hide" id="div_endereco">
                                <label for="endereco">Endereço para entrega</label>
                                <input type="text" id="endereco" class="form-control" name="endereco">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-fw fa-close"></i>
                        Fechar
                    </button>
                    <button type="submit" class="btn btn-primary">Requisitar emissão</button>
                </div>
            </form>
        </div>
    </div>
</div>