<!-- Modal -->
<div class="modal fade" id="modal-anexar-arquivo" role="dialog" aria-labelledby="modal-anexar-arquivo-label">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-anexar-arquivo-label"><i class="fa fa-fw fa-upload"></i> Anexar documento</h4>
            </div>
            <form 
                id="form-anexar-arquivo" 
                action="{{ route('cliente.anexar-arquivo.store') }}" 
                method="POST"
                enctype="multipart/form-data"
            >
                {{ csrf_field() }}
                <input type="hidden" name="servico_id" />
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="nome">Nome</label>
                                <input type="text" class="form-control" id="nome" placeholder="nome do documento" name="nome" required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="arquivo">Documento</label>
                                <input type="file" id="arquivo" name="documento" required>
                                <p class="help-block">Envie-nos o documento digitalizado e em formato JPG ou PDF</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-fw fa-close"></i>
                        Fechar
                    </button>
                    <button type="submit" class="btn btn-primary">Anexar Documento</button>
                </div>
            </form>
        </div>
    </div>
</div>