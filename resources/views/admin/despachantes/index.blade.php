@extends('adminlte::page') 

@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="box box-solid">
                <div class="box-header">
                    <h3 class="box-title">
                        Listagem de Despachantes
                    </h3>
                    <a role="button" 
                        class="btn btn-primary"
                        href="{{ route('despachantes.create') }}"
                    >
                        <i class="fa fa-fw fa-plus"></i>
                        Adicionar novo despachante
                    </a>
                </div>
                <div class="box-body">        
                    <table id="tabela-despachantes" class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>Celular</th>
                                <th>Email</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $despachantes as $despachante )
                            <tr>
                                <td>
                                    @if ( $despachante->status === 'AGUARDANDO APROVACAO' )
                                        <span class="label label-warning"><i class="fa fa-fw fa-clock-o"></i></span> &nbsp;
                                    @elseif( $despachante->status === 'APROVADO' )
                                        <span class="label label-success"><i class="fa fa-fw fa-check"></i></span> &nbsp;
                                    @elseif( $despachante->status === 'NEGADO')
                                        <span class="label label-danger"><i class="fa fa-fw fa-close"></i></span> &nbsp;
                                    @endif 
                                    {{ $despachante->nome }}
                                </td>
                                <td>{{ $despachante->telefone }}</td>
                                <td>{{ $despachante->celular }}</td>
                                <td>{{ $despachante->email }}</td>
                                <td class="text-right">
                                    @if( $despachante->status === 'AGUARDANDO APROVACAO' )
                                        <button type="button" 
                                            id="btn-habilitar-despachante-{{ $despachante->id }}"
                                            class="btn btn-xs btn-success"
                                            onclick="habilitarDespachante({{ $despachante->id }})"      
                                            >
                                                <i class="fa fa-fw fa-check"></i>
                                                habilitar acesso
                                        </button>
                                        <button type="button" 
                                            id="btn-desabilitar-despachante-{{ $despachante->id }}"
                                            class="btn btn-xs btn-danger" 
                                            onclick="desabilitarDespachante({{ $despachante->id }})"      
                                            >
                                                <i class="fa fa-fw fa-exclamation-circle"></i>
                                                negar acesso
                                        </button>
                                    @elseif( $despachante->status === 'APROVADO' )
                                        <button type="button" 
                                            id="btn-desabilitar-despachante-{{ $despachante->id }}"
                                            class="btn btn-xs btn-danger" 
                                            onclick="desabilitarDespachante({{ $despachante->id }})"      
                                            >
                                                <i class="fa fa-fw fa-exclamation-circle"></i>
                                                negar acesso
                                        </button>

                                    @elseif( $despachante->status === 'NEGADO' )
                                        <button type="button" 
                                            id="btn-habilitar-despachante-{{ $despachante->id }}"
                                            class="btn btn-xs btn-success"
                                            data-loading-text='<i class="fa fa-fw fa-spinner"></i> carregando...'  
                                            onclick="habilitarDespachante({{ $despachante->id }})"      
                                            >
                                                <i class="fa fa-fw fa-check"></i>
                                                habilitar acesso
                                        </button>
                                    @endif
                                    <button type="button" 
                                            id="btn-exibir-despachante-{{ $despachante->id }}"
                                            class="btn btn-xs btn-primary"
                                            data-loading-text='<i class="fa fa-fw fa-spinner"></i> carregando...'  
                                            onclick="exibirDespachante({{ $despachante->id }})"      
                                    >
                                        <i class="fa fa-fw fa-address-card"></i>
                                        visualizar
                                    </button>
                                    <a role="button" 
                                        class="btn btn-xs btn-warning"
                                        href="{{ route('despachantes.edit', ['id' => $despachante->id ]) }}"
                                    >
                                        <i class="fa fa-fw fa-edit"></i>
                                        editar
                                    </a>
                                    <button type="button" 
                                            class="btn btn-xs btn-danger"
                                            onclick="deletarDespachante({{ $despachante->id }})">
                                        <i class="fa fa-fw fa-trash"></i>
                                        apagar
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="box box-solid">
                <div class="box-body">
                    <ul class="nav nav-pills">
                        <li role="presentation">
                            <span class="label label-warning">
                                <i class="fa fa-fw fa-clock-o"></i>
                            </span> &nbsp; Aguardando aprovação &nbsp;
                        </li>
                        <li role="presentation">
                            <span class="label label-success">
                                <i class="fa fa-fw fa-check"></i>
                            </span> &nbsp; Aprovado &nbsp;
                        </li>
                        <li role="presentation">
                            <span class="label label-danger">
                                <i class="fa fa-fw fa-close"></i>
                            </span> &nbsp; Negado &nbsp;
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>

    @include('admin.despachantes.includes.form-deletar-despachante')
    @include('admin.despachantes.includes.modal-exibir-despachante')
@endsection 