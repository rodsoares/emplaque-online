@extends('adminlte::page')

@section('content')
    <div class="box box-solid">
        <div class="box-header">
            <h3 class="box-title">Formulário de cadastro de despachante</h3>
        </div>
        <div class="box-body">
            <form action="{{ route('despachantes.update', ['id' => $despachante->id ]) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="row">
                    <div class="form-group col-sm-5">
                        <label for="nome">Nome do Despachante</label>
                        <input type="text" 
                               class="form-control obrigatorio" 
                               placeholder="Nome/Razão Social do Despachante"
                               name="nome"
                               value="{{ $despachante->nome }}"
                               >
                    </div>

                    <div class="form-group col-sm-4">
                        <label for="email">Email</label>
                        <input type="email" 
                               id="email" 
                               class="form-control obrigatorio" 
                               placeholder="despachante@email.com.br"
                               name="email"
                               value="{{ $despachante->email }}"
                               >
                    </div>

                    <div class="form-group col-sm-3">
                        <label for="status">Status</label>
                        <select id="status" class="form-control" name="status" >
                            <option value="AGUARDANDO APROVACAO" @if( $despachante->status === 'AGUARDANDO APROVACAO') selected @endif>Aguardando Aprovação</option>
                            <option value="APROVADO" @if( $despachante->status === 'APROVADO') selected @endif>Aprovado</option>
                            <option value="NEGADO" @if( $despachante->status === 'NEGADO') selected @endif>Negado</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-sm-2">
                        <label for="telefone">Tel. Residencial</label>
                        <input type="text" 
                               id="telefone" 
                               class="form-control obrigatorio" 
                               data-mask="(00) 0000-0000"
                               name="telefone"
                               value="{{ $despachante->telefone }}"
                               >
                    </div>

                    <div class="form-group col-sm-2">
                        <label for="celular">Tel. Celular</label>
                        <input type="text" 
                               id="celular" 
                               class="form-control obrigatorio" 
                               data-mask="(00) 00000-0000"
                               name="celular"
                               value="{{ $despachante->celular }}"
                               >
                    </div>

                    <div class="form-group col-sm-3">
                        <label for="municipio">Município</label>
                        <select id="municipio" class="form-control" name="municipio_codigo">
                            @foreach( $municipios as $municipio )
                                <option value="{{ $municipio->Codigo }}" @if( $municipio->Codigo === $despachante->municipio_codigo) selected @endif>{{ $municipio->Nome }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-sm-5">
                        <label for="endereco">Endereço</label>
                        <input type="text" 
                               id="endereco" 
                               class="form-control obrigatorio" 
                               name="endereco"
                               value="{{ $despachante->endereco }}"
                               >
                    </div>
                </div>

                 <div class="form-group">
                    <label for="logotipo-despachante">Logotipo</label>
                    <input type="file" id="logotipo-despachante" name="logo">
                    <p class="help-block">Escolha o logotipo do despachante em .JPG ou PNG</p>
                </div>

                <div class="text-right">
                    <a role="button" class="btn btn-danger"
                        href="{{ route('despachantes.index') }}"
                    >
                        <i class="fa fa-fw fa-close"></i>
                        Cancelar
                    </a>

                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-fw fa-check"></i>
                        Atualizar despachante
                    </button>
                </div>
            </form>
        </div>
    </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js" integrity="sha256-u7MY6EG5ass8JhTuxBek18r5YG6pllB9zLqE4vZyTn4=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $('select.form-control').select2();
        });
    </script>
@stop