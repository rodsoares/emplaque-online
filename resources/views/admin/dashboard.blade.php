@extends('adminlte::page') @section('content')

<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3>{{ $total_clientes}}</h3>

                <p>Clientes</p>
            </div>
            <div class="icon">
                <i class="fa fa-users"></i>
            </div>
            <a href="/admin/usuarios/?clientes" class="small-box-footer">Informações
                <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3>{{ $total_despachantes}}</h3>

                <p>Despachantes</p>
            </div>
            <div class="icon">
                <i class="fa fa-address-card-o"></i>
            </div>
            <a href="{{ route('despachantes.index') }}" class="small-box-footer">Informações
                <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3>{{ $total_servicos_em_aberto }}</h3>

                <p>Serviços em Aberto</p>
            </div>
            <div class="icon">
                <i class="fa fa-hourglass-1"></i>
            </div>
                <a href="{{ route('admin.servicos') }}" class="small-box-footer">Informações
                <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3>{{ $total_servicos_realizados}}</h3>

                <p>Serviços Realizados</p>
            </div>
            <div class="icon">
                <i class="fa fa-check-square-o"></i>
            </div>
            <a href="{{ route('admin.servicos') }}" class="small-box-footer">Informações
                <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-purple">
            <div class="inner">
                <h3>{{ $novos_despachantes }}</h3>

                <p>Novos Despachantes</p>
            </div>
            <div class="icon">
                <i class="fa fa-address-card-o"></i>
            </div>
            <a href="{{ route('despachantes.index') }}" class="small-box-footer">Informações
                <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div>
</div>
@endsection