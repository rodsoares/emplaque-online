@extends('adminlte::page')

@section('content')
    <div class="box box-solid">
        <div class="box-header">
            <h3 class="box-title">Formulário de cadastro de usuario</h3>
        </div>
        <div class="box-body">
            <form action="{{ route('usuarios.store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="form-group col-sm-3">
                        <label for="name">Nome do usuário</label>
                        <input type="text" 
                               class="form-control obrigatorio" 
                               placeholder="Name do usuário"
                               name="name"
                               required>
                    </div>

                    <div class="form-group col-sm-3">
                        <label for="email">Email</label>
                        <input type="email" 
                               id="email" 
                               class="form-control obrigatorio" 
                               placeholder="usuario@email.com.br"
                               name="email"
                               required>
                    </div>

                    <div class="form-group col-sm-3">
                        <label for="password">Senha</label>
                        <input type="password" 
                               id="password" 
                               class="form-control obrigatorio" 
                               name="password"
                               required>
                    </div>

                    <div class="form-group col-sm-3">
                        <label for="tipo">Status</label>
                        <select id="tipo" class="form-control" name="tipo" required>
                            <option value="administrador" selected>Administrador</option>
                            <option value="despachante">Despachante</option>
                        </select>
                    </div>
                </div>

                <div class="text-right">
                    <button type="button" class="btn btn-danger"
                        onclick="javascript:history.back()"
                    >
                        <i class="fa fa-fw fa-close"></i>
                        Cancelar
                    </button>

                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-fw fa-check"></i>
                        Adicionar usuario
                    </button>
                </div>
            </form>
        </div>
    </div>
@stop

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js" integrity="sha256-u7MY6EG5ass8JhTuxBek18r5YG6pllB9zLqE4vZyTn4=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $('select.form-control').select2();
        });
    </script>
@stop