@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="box box-solid">
                <div class="box-header">
                    <h3 class="box-title">
                        Listagem de Usuários
                    </h3>
                    <a role="button" 
                        class="btn btn-primary"
                        href="{{ route('usuarios.create') }}"
                    >
                        <i class="fa fa-fw fa-plus"></i>
                        Adicionar novo usuário
                    </a>
                </div>
                <div class="box-body">        
                    <table id="tabela-usuarios" class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $usuarios as $usuario )
                            <tr>
                                <td>
                                    {{ $usuario->name }} &nbsp;
                                    @if ( count($usuario->getRoleNames()) )
                                        <span class="label label-default">{{ $usuario->getRoleNames()[0]}}</span>
                                    @endif
                                </td>
                                <td>{{ $usuario->email }}</td>
                                <td class="text-right">
                                    <a role="button" 
                                        class="btn btn-xs btn-warning"
                                        href="{{ route('usuarios.edit', ['id' => $usuario->id]) }}"
                                    >
                                        <i class="fa fa-fw fa-edit"></i>
                                        editar
                                    </a>
                                    <button type="button" 
                                            class="btn btn-xs btn-danger"
                                            onclick="deletarUsuario({{$usuario->id}})">
                                        <i class="fa fa-fw fa-trash"></i>
                                        apagar
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="box box-solid">
                <div class="box-body">
                    <ul class="nav nav-pills">
                        <li role="presentation">
                            <span class="label label-warning">
                                <i class="fa fa-fw fa-clock-o"></i>
                            </span> &nbsp; Aguardando aprovação &nbsp;
                        </li>
                        <li role="presentation">
                            <span class="label label-success">
                                <i class="fa fa-fw fa-check"></i>
                            </span> &nbsp; Aprovado &nbsp;
                        </li>
                        <li role="presentation">
                            <span class="label label-danger">
                                <i class="fa fa-fw fa-close"></i>
                            </span> &nbsp; Negado &nbsp;
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @include('admin.usuarios.includes.form-deletar-usuario')
@stop