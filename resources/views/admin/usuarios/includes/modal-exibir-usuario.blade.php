<!-- Modal -->
<div class="modal fade" id="modal-exibir-despachante" tabindex="-1" role="dialog" aria-labelledby="modal-exibir-despachante-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-exibir-despachante-label"></h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">
                    <i class="fa fa-fw fa-close"></i>
                    Fechar
                </button>
            </div>
        </div>
    </div>
</div>