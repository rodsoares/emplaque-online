@extends('layouts.front')

@section('content')
    <div class="row text-center">
        <div class="col-sm-12 col-md-12 col-lg-12 text-center">
            <h1>Consultar Débitos</h1>
            <p class="lead text-muted">
                Informe abaixo algumas informações para realizamos a consulta <br> 
                no sistema do DETRAN de Santa Catarina acerca dos débitos de seu veículo.
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-5 col-lg-5 col-center-block">
            <div class="box box-solid">
                <div class="box-body">
                    <form class="form-group" method="POST" id="captcha-form" name="captcha-form" action="{{ route('consultar-debitos') }}"
                          style="margin: 20px 20px 20px 20px;">
                          {{ csrf_field() }}
                        <div class="">
                            <div class="form-group">
                                <input type="text" name="placa" id="placa" class="form-control" placeholder="Placa do seu Veículo" minlength="7"
                                    maxlength="8" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="renavam" id="renavam" class="form-control" placeholder="Número do seu RENAVAM" minlength="9"
                                    maxlength="11" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="telefone" id="telefone" class="form-control " placeholder="Deixe aqui seu contato ( residencial ou celular)"
                                    maxlength="255" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="email" id="email" class="form-control " placeholder="Deixe aqui seu email"
                                    maxlength="255" required>
                            </div>
                            <div class="form-group" id="termos-de-usodiv">
                                <input type="checkbox" name="termos-de-uso" id="termos-de-uso" class="f-input input-field" required>
                                <label for="termos-de-uso">Concordo com os
                                    <a href="#">termos de uso</a>.
                                </label>
                            </div>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary btn-block">
                                Consultar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/busy-load/dist/app.min.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .col-center-block {
            float: none;
            display: block;
            margin: 0 auto;
            /* margin-left: auto; margin-right: auto; */
        }
    </style>
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/busy-load/dist/app.min.js"></script>
    <script>
        $('#captcha-form').submit(function(event){
            // with options
            $.busyLoadFull("show", {
                color: "black", 
                spinner: "cube-grid",
                text: "Carregando informações ...",
                fontSize: "2rem",
                animation: "slide"
            });
        });
    </script>
@endsection