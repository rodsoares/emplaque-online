$(document).ready(function() {
    var tabela_usuarios =  $('#tabela-usuarios').DataTable({
         "language": {
             "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
         }
    });
});

function exibirUsuario( id ) {
    var modal = $('#modal-exibir-usuario');
    var btn_exibir = $('#btn-exibir-usuario-'+id);

    btn_exibir.button('loading');

    /** obtendo dados */
    $.get('/endpoints/usuarios/' + id)
     .done(function( response ){
        var title = $('#modal-exibir-usuario #modal-exibir-usuario-label');
        var body  = $('#modal-exibir-usuario .modal-body');

        /** primeira linha */
        var html = '<div class="row">';

        html += '<div class="col-sm-12 col-md-5 col-lg-5">';
        html += '<a href="#" class="thumbnail">';
        html += '<img src="/storage/'+ response.logo +'" alt="...">';
        html += '</a>';
        html += '</div>';

        html += '<div class="col-sm-12 col-md-7 col-lg-7">';
        html += '<dl>';
        html += '<dt>Nome:</dt>';
        html += '<dd>'+ response.nome +'</dd>';
        html += '<dt>Email:</dt>';
        html += '<dd>'+ response.email +'</dd>';
        html += '<div class="row">';
        html += '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">';
        html += '<dt>Telefone:</dt>';
        html += '<dd>'+ response.telefone +'</dd>';
        html += '</div>';
        html += '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">';
        html += '<dt>Celular:</dt>';
        html += '<dd>'+ response.celular +'</dd>';
        html += '</div>';
        html += '</div>';
        html += '</dl>';
        html += '</div>';

        html += '</div>';
        
        /** Segunda linha */
        html += '<div class="row">';

        html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
        html += '<div class="box box-solid">';
        html += '<div class="box-body"><table class="table table-condensed table-bordered table-striped"><thead><tr class="bg-info"><th colspan="3" class="text-center">Serviços em Aberto</th></tr><tr><th>Cliente</th><th class="text-center">Serviço</th><th class="text-right">Status</th></tr></tr></thead><tbody><tr></tr></tbody></table></div></div>';
        html += '</div>';
        html += '</div>';

        html += '</div>';
        
        title.html('usuario ' + response.nome);
        body.html(html);

        modal.modal('show');
     });

    modal.on('hidden.bs.modal', function () {
        btn_exibir.button('reset');
    });
}

function deletarUsuario( id ) {
    swal({
        title: "Você tem certeza ?",
        text: "Uma vez excluído você não será capaz de recuperá-lo!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            var form = $('#form-deletar-usuario');
            var url = location.href + '/' + id;
        
            document.querySelector('#form-deletar-usuario').reset(); 
            $('#form-deletar-usuario').attr('action', url);
            $('#form-deletar-usuario').submit();
        }
    });
}