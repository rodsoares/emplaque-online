$(document).ready(function() {
    var tabela_despachantes =  $('#tabela-despachantes').DataTable({
         "language": {
             "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
         }
    });
});

function exibirDespachante( id ) {
    var modal = $('#modal-exibir-despachante');
    var btn_exibir = $('#btn-exibir-despachante-'+id);

    btn_exibir.button('loading');

    /** obtendo dados */
    $.get('/endpoints/despachantes/' + id)
     .done(function( response ){
        var title = $('#modal-exibir-despachante #modal-exibir-despachante-label');
        var body  = $('#modal-exibir-despachante .modal-body');

        /** primeira linha */
        var html = '<div class="row">';

        html += '<div class="col-sm-12 col-md-5 col-lg-5">';
        html += '<a href="#" class="thumbnail">';
        if ( response.logo != null ) {
            html += '<img src="/uploads/'+ response.logo +'" class="img-responsive" alt="...">';
        } else {
            html += '<img src="http://placehold.it/200x110" class="img-responsive" alt="...">';
        }
        html += '</a>';
        html += '</div>';

        html += '<div class="col-sm-12 col-md-7 col-lg-7">';
        html += '<dl>';
        html += '<dt>Nome:</dt>';
        html += '<dd>'+ response.nome +'</dd>';
        html += '<dt>Email:</dt>';
        html += '<dd>'+ response.email +'</dd>';
        html += '<div class="row">';
        html += '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">';
        html += '<dt>Telefone:</dt>';
        html += '<dd>'+ response.telefone +'</dd>';
        html += '</div>';
        html += '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">';
        html += '<dt>Celular:</dt>';
        html += '<dd>'+ response.celular +'</dd>';
        html += '</div>';
        html += '</div>';
        html += '</dl>';
        html += '</div>';

        html += '</div>';
        
        /** Segunda linha */
        html += '<div class="row">';

        html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
        html += '<div class="box box-solid">';
        html += '<div class="box-body"><table class="table table-condensed table-bordered table-striped"><thead><tr class="bg-info"><th colspan="3" class="text-center">Serviços em Aberto</th></tr><tr><th>Cliente</th><th class="text-center">Serviço</th><th class="text-right">Status</th></tr></tr></thead><tbody><tr></tr></tbody></table></div></div>';
        html += '</div>';
        html += '</div>';

        html += '</div>';
        
        title.html('Despachante ' + response.nome);
        body.html(html);

        modal.modal('show');
     });

    modal.on('hidden.bs.modal', function () {
        btn_exibir.button('reset');
    });
}

function deletarDespachante( id ) {
    swal({
        title: "Você tem certeza ?",
        text: "Uma vez excluído você não será capaz de recuperá-lo!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            var form = $('#form-deletar-despachante');
            var url = location.href + '/' + id;
        
            document.querySelector('#form-deletar-despachante').reset(); 
            $('#form-deletar-despachante').attr('action', url);
            $('#form-deletar-despachante').submit();
        }
    });
}

function habilitarDespachante( id ) {
    var form = $('#form-deletar-despachante');
    var url = location.href + '/habilitar/' + id;
        
    document.querySelector('#form-deletar-despachante').reset(); 

    $('#form-deletar-despachante').attr('action', url);
    $('#form-deletar-despachante input[name="_method"]').val('POST');
    $('#form-deletar-despachante').submit();
}

function desabilitarDespachante( id ) {
    var form = $('#form-deletar-despachante');
    var url = location.href + '/desabilitar/' + id;
        
    document.querySelector('#form-deletar-despachante').reset(); 

    $('#form-deletar-despachante').attr('action', url);
    $('#form-deletar-despachante input[name="_method"]').val('POST');
    $('#form-deletar-despachante').submit();
}