$('#modal-requisitar-servico').on('shown.bs.modal', function () {

    /**
     * Obtendo valores do servidor e populando select
     */
    var municipio = $('#form-requisitar-servico #municipio');

    $.get('/api/municipios/SC?to-select2')
     .done( function(response) {
        municipio.html(response)
     })

    municipio.select2();

    /**
     * alterando valores do select sempre um novo município for selecionado
     */
    municipio.change( function (event) {
        $.get('/api/municipios/'+ $(this).val() + '/despachantes/?to-select2')
         .done(function(response){
             var despachante = $('#form-requisitar-servico #despachante')
            
             despachante.html(response)
             despachante.attr('readonly', 'false') 
             despachante.select2();
         })
         .fail(function(){
            var despachante = $('#form-requisitar-servico #despachante')
            despachante.attr('readonly', 'false')
            despachante.html('')
            despachante.select2();
         })
    })
})

$('#modal-requisitar-servico').on('hidden.bs.modal', function () {
    var div = $('#div_endereco');
    if ( !div.hasClass('hide') )
        div.addClass('hide');
});

$('input[name="a_domicilio"]').change(function(event){
    var div = $('#div_endereco');
    if ( div.hasClass('hide') )
    {
        div.removeClass('hide');
    } else {
        div.addClass('hide');
    }
});

/**
 * Funções gerais
 */

function anexarArquivo( servico_id ) {
    var modal = $("#modal-anexar-arquivo");  
    // zerando valores anteriores
    document.querySelector('#form-anexar-arquivo').reset(); 
    // atribuindo id do serviço em questão
    $("#modal-anexar-arquivo #form-anexar-arquivo input[name='servico_id']").attr('value', +servico_id);  
    // exibindo o dito cujo
    modal.modal('show');
} 