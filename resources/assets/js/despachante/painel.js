function abrirDetalhes(id_servico = null) {
    
    if ( id_servico )
    {
        var linha = $("#detalhes-servico-"+id_servico);
        var btn = $("#btn-abrir-detalhes-"+id_servico);


        if ( linha.hasClass('hidden') ){
            linha.removeClass('hidden');
            btn.html('<i class="fa fa-fw fa-minus"></i>')
        } else {
            linha.addClass('hidden');
            btn.html('<i class="fa fa-fw fa-plus"></i>')
        }
    } else {
        /** obtendo dos ids dos servicos */
        $('td[name="id"]').each(function (index, value) {
            var id_servico = +($( value ).html());

            var linha = $("#detalhes-servico-"+id_servico);
            var btn = $("#btn-abrir-detalhes-"+id_servico);


            if ( linha.hasClass('hidden') ){
                linha.removeClass('hidden');
                btn.html('<i class="fa fa-fw fa-minus"></i>')
            } else {
                linha.addClass('hidden');
                btn.html('<i class="fa fa-fw fa-plus"></i>')
            }
        });
    } 
}

function abrirModalAlterarStatus( servico_id ) {
    var modal = $("#modal-alterar-status");  
    // zerando valores anteriores
    document.querySelector('#form-alterar-status').reset(); 
    // atribuindo id do serviço em questão
    $("#modal-alterar-status #form-alterar-status input[name='servico_id']").attr('value', +servico_id);  
    // exibindo o dito cujo
    modal.modal('show');
}