<?php

namespace Emplaque\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class UsuarioValidator.
 *
 * @package namespace Emplaque\Validators;
 */
class UsuarioValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'filled|string|max:255',
            'email' => 'filled|string|email|max:255',
        ],
    ];
}
