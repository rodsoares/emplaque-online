<?php

namespace Emplaque\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Prettus\Repository\Contracts\Presentable;
use Prettus\Repository\Traits\PresentableTrait;

/**
 * Class Consulta.
 *
 * @package namespace Emplaque\Models;
 */
class Consulta extends Model implements Transformable, Presentable
{
    use TransformableTrait, PresentableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'placa',
        'renavam',
        'email',
        'telefone'
    ];


}
