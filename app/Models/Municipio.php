<?php

namespace Emplaque\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Municipio.
 *
 * @package namespace Emplaque\Models;
 */
class Municipio extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'Municipio';
    protected $primaryKey = 'Id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public function estado()
    {
        return $this->belongsTo(Estado::class, 'Uf');
    }

    public function despachantes()
    {
        return $this->hasMany(Despachante::class, 'municipio_codigo', 'Codigo');
    }

}
