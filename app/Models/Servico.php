<?php

namespace Emplaque\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Servico.
 *
 * @package namespace Emplaque\Models;
 */
class Servico extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'despachante_id',
        'placa',
        'renavam',
        'email',
        'tel_contato',
        'a_domicilio',
        'endereco',
        'status'
    ];

    public function cliente()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function despachante()
    {
        return $this->belongsTo(Despachante::class, 'despachante_id');
    }

    public function arquivos()
    {
        return $this->hasMany(Arquivo::class);
    }
}
