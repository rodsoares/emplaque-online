<?php

namespace Emplaque\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Despachante.
 *
 * @package namespace Emplaque\Models;
 */
class Despachante extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'logo',
        'status',
        'email',
        'telefone',
        'celular',
        'municipio_codigo',
        'endereco'
    ];

    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function servicos()
    {
        return $this->hasMany(Servico::class, 'despachante_id');
    }

    public function municipio()
    {
        return $this->belongsTo(Municipio::class, 'municipio_codigo', 'Codigo');
    }

}
