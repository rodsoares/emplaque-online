<?php

namespace Emplaque\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Estado.
 *
 * @package namespace Emplaque\Models;
 */
class Estado extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'Municipio';
    protected $primaryKey = 'Id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public function municipios()
    {
        return $this->hasMany(Municipio::class, 'Uf');
    }

}
