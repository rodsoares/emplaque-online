<?php

namespace Emplaque\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Arquivo.
 *
 * @package namespace Emplaque\Models;
 */
class Arquivo extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'servico_id',
        'nome',
        'caminho'
    ];

    public function servico()
    {
        return $this->belongsTo(Servico::class);
    }

}
