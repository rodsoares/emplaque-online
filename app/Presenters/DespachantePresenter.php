<?php

namespace Emplaque\Presenters;

use Emplaque\Transformers\DespachanteTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class DespachantePresenter.
 *
 * @package namespace Emplaque\Presenters;
 */
class DespachantePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new DespachanteTransformer();
    }
}
