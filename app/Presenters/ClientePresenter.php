<?php

namespace Emplaque\Presenters;

use Emplaque\Transformers\ClienteTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ClientePresenter.
 *
 * @package namespace Emplaque\Presenters;
 */
class ClientePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ClienteTransformer();
    }
}
