<?php

namespace Emplaque\Presenters;

use Emplaque\Transformers\ConsultaTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ConsultaPresenter.
 *
 * @package namespace Emplaque\Presenters;
 */
class ConsultaPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ConsultaTransformer();
    }
}
