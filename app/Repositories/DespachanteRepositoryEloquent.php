<?php

namespace Emplaque\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Emplaque\Repositories\Interfaces\DespachanteRepository;
use Emplaque\Models\Despachante;
use Emplaque\Validators\DespachanteValidator;

/**
 * Class DespachanteRepositoryEloquent.
 *
 * @package namespace Emplaque\Repositories;
 */
class DespachanteRepositoryEloquent extends BaseRepository implements DespachanteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Despachante::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Upload do arquivo enviado
     */
    public function upload( $file )
    {
        $nome = $file->getClientOriginalName();
        return $file->storeAs('despachantes/logos', $nome, 'public');
    }  
}
