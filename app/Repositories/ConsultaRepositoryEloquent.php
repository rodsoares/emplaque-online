<?php

namespace Emplaque\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Emplaque\Repositories\Interfaces\ConsultaRepository;
use Emplaque\Presenters\ConsultaPresenter;
use Emplaque\Models\Consulta;
use Emplaque\Validators\ConsultaValidator;

/**
 * Class ConsultaRepositoryEloquent.
 *
 * @package namespace Emplaque\Repositories;
 */
class ConsultaRepositoryEloquent extends BaseRepository implements ConsultaRepository
{
    
    protected $skipPresenter = true;
    
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Consulta::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ConsultaValidator::class;
    }

    public function presenter()
    {
        return ConsultaPresenter::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
