<?php

namespace Emplaque\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Emplaque\Repositories\Interfaces\UsuarioRepository;
use Emplaque\Models\User as Usuario;
use Emplaque\Validators\UsuarioValidator;

/**
 * Class UsuarioRepositoryEloquent.
 *
 * @package namespace Emplaque\Repositories;
 */
class UsuarioRepositoryEloquent extends BaseRepository implements UsuarioRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Usuario::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    private function createUserIfnotExists()
    {

    }
    
}
