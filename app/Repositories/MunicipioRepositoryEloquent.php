<?php

namespace Emplaque\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Emplaque\Repositories\Interfaces\MunicipioRepository;
use Emplaque\Models\Municipio;
use Emplaque\Validators\MunicipioValidator;

/**
 * Class MunicipioRepositoryEloquent.
 *
 * @package namespace Emplaque\Repositories;
 */
class MunicipioRepositoryEloquent extends BaseRepository implements MunicipioRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Municipio::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
