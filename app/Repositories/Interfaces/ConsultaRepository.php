<?php

namespace Emplaque\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ConsultaRepository.
 *
 * @package namespace Emplaque\Interfaces;
 */
interface ConsultaRepository extends RepositoryInterface
{
    //
}
