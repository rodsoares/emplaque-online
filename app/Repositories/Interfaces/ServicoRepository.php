<?php

namespace Emplaque\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ServicoRepository.
 *
 * @package namespace Emplaque\Repositories\Interfaces;
 */
interface ServicoRepository extends RepositoryInterface
{
    //
}
