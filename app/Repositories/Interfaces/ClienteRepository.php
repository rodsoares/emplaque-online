<?php

namespace Emplaque\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ClienteRepository.
 *
 * @package namespace Emplaque\Repositories\Interfaces;
 */
interface ClienteRepository extends RepositoryInterface
{
    //
}
