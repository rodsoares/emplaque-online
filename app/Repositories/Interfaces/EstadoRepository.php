<?php

namespace Emplaque\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EstadoRepository.
 *
 * @package namespace Emplaque\Repositories\Interfaces;
 */
interface EstadoRepository extends RepositoryInterface
{
    //
}
