<?php

namespace Emplaque\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DespachanteRepository.
 *
 * @package namespace Emplaque\Repositories\Interfaces;
 */
interface DespachanteRepository extends RepositoryInterface
{
    public function upload( $file );
}
