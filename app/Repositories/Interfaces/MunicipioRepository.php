<?php

namespace Emplaque\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MunicipioRepository.
 *
 * @package namespace Emplaque\Repositories\Interfaces;
 */
interface MunicipioRepository extends RepositoryInterface
{
    //
}
