<?php

namespace Emplaque\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ArquivoRepository.
 *
 * @package namespace Emplaque\Repositories\Interfaces;
 */
interface ArquivoRepository extends RepositoryInterface
{
    //
}
