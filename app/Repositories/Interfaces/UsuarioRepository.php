<?php

namespace Emplaque\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UsuarioRepository.
 *
 * @package namespace Emplaque\Repositories\Interfaces;
 */
interface UsuarioRepository extends RepositoryInterface
{
    //
}
