<?php

namespace Emplaque\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Emplaque\Repositories\Interfaces\ArquivoRepository;
use Emplaque\Models\Arquivo;
use Emplaque\Validators\ArquivoValidator;

/**
 * Class ArquivoRepositoryEloquent.
 *
 * @package namespace Emplaque\Repositories;
 */
class ArquivoRepositoryEloquent extends BaseRepository implements ArquivoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Arquivo::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
    /**
     * Upload do arquivo enviado
     */
    public function upload( $file )
    {
        $nome = $file->getClientOriginalName();
        return $file->storeAs('servicos/documentos', $nome, 'public');
    }
}
