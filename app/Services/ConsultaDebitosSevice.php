<?php

namespace Emplaque\Services;

use Goutte\Client as GoutteClient;
use GuzzleHttp\Client as GuzzleClient;

use AntiCaptcha\NoCaptchaProxyLess;


class ConsultaDebitosService 
{
    protected $url;
    protected $sitekey;
    protected $key;

    public function __construct(GoutteClient $goutte, GuzzleClient $guzzle) 
    {
        /**
         * Não é a melhor forma de setar valores de configuração!
         * TODO: REFATORAR ESSA PARTE. Talvez movendo essas conf para o arquivo .env ajude.
         */
        $this->cliente = $goutte;
        $this->guzzle = $guzzle;
        $this->url = "http://consultas.detrannet.sc.gov.br/servicos/consultaveiculo.asp";
        $this->sitekey = "6LeJOzMUAAAAAMxYRUSoKwySawDtIarnMxUR8fJq";
        $this->key = "d025c0a05117a46a99735aa33aea178f";
    }

    public function consultar($placa, $renavam) {
        $opt['placa'] = $placa;
        $opt['renavam'] = $renavam;

        // Obtendo g-recaptcha-response 
        $info = ($this->getTaskInfo($opt))['resultado'];
        
        // acessando a página já passando o recaptcha
        $parametros = "?placa=$placa&renavam=$renavam&g-recaptcha-response=$info&Submit=Consultar&modo=C";

        $crawler = $this->cliente->request('GET', $this->url . $parametros );
        
        /** Obtendo dados acerca das informações gerais do veículo */
        $retorno['informacoes_gerais'] = $crawler->filter('#div_servicos_02 table tr')->each(function($tr) {
            return $tr->filter('td')->each(function ($td) {
                $valor = $td->filter('span')->text();
                $valor = trim(preg_replace('/[\s\t\n\r\s]+/', ' ', $valor));

                return $valor;
            });
        });

        /** Obtendo dados de 'Infrações em Autuação' */ 
        $retorno['infracoes_em_autuacoes'] = $crawler->filter('#div_servicos_10 tr td.bordaAbaixoFina.celnlef8')->each(function($node) {
            $result = $node->text();
            return trim(preg_replace('/[\s\t\n\r\s]+/', ' ', $result));
        });

        /** Obtendo dados de 'Débitos' */
        $retorno['debitos']['geral'] = $crawler->filter('#div_servicos_03 table:nth-child(2) tr')->each(function($node) {
            return  $node->filter('td.titcel')->each(function($td){
                return trim(preg_replace('/[\s\t\n\r\s]+/', ' ', $td->text()));
            });
        });

        $retorno['debitos']['detalhado'] = $crawler->filter('#div_servicos_03 table:nth-child(1) tr:nth-child(n+1)')->each(function($node) {
            return $node->filter('td:nth-child(n+1)')->each( function($td) {
                return $td->text();
            });
        });

        return $retorno;
    }

    protected function getTaskInfo(array $opt)
    {
        $placa = $opt['placa'];
        $renavam = $opt['renavam'];

        $_task = new NoCaptchaProxyLess();
        $_task->setKey($this->key);
        $_task->setWebsiteKey( $this->sitekey );
        $_task->setWebsiteUrl( $this->url . "?placa=$placa" . "&renavam=$renavam" );

        $task = 0;
        $result = 0;

        while (!$_task->createTask()) {
            $_task->createTask();
        }

        $task = $_task->getTaskId();

        while (!$_task->waitForResult()) {
            $_task->waitForResult();
        }

        $result = $_task->getTaskSolution();
        
        return [ "task" => $task, "resultado" => $result ];
    }
}