<?php

namespace Emplaque\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\Emplaque\Repositories\Interfaces\ConsultaRepository::class, \Emplaque\Repositories\ConsultaRepositoryEloquent::class);
        $this->app->bind(\Emplaque\Repositories\Interfaces\DespachanteRepository::class, \Emplaque\Repositories\DespachanteRepositoryEloquent::class);
        $this->app->bind(\Emplaque\Repositories\Interfaces\ClienteRepository::class, \Emplaque\Repositories\ClienteRepositoryEloquent::class);
        $this->app->bind(\Emplaque\Repositories\Interfaces\UsuarioRepository::class, \Emplaque\Repositories\UsuarioRepositoryEloquent::class);
        $this->app->bind(\Emplaque\Repositories\Interfaces\ServicoRepository::class, \Emplaque\Repositories\ServicoRepositoryEloquent::class);
        $this->app->bind(\Emplaque\Repositories\Interfaces\MunicipioRepository::class, \Emplaque\Repositories\MunicipioRepositoryEloquent::class);
        $this->app->bind(\Emplaque\Repositories\Interfaces\ArquivoRepository::class, \Emplaque\Repositories\ArquivoRepositoryEloquent::class);
        //:end-bindings:
    }
}
