<?php

namespace Emplaque\Transformers;

use League\Fractal\TransformerAbstract;
use Emplaque\Models\Despachante;

/**
 * Class DespachanteTransformer.
 *
 * @package namespace Emplaque\Transformers;
 */
class DespachanteTransformer extends TransformerAbstract
{
    /**
     * Transform the Despachante entity.
     *
     * @param \Emplaque\Models\Despachante $model
     *
     * @return array
     */
    public function transform(Despachante $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
