<?php

namespace Emplaque\Transformers;

use League\Fractal\TransformerAbstract;
use Emplaque\Models\Cliente;

/**
 * Class ClienteTransformer.
 *
 * @package namespace Emplaque\Transformers;
 */
class ClienteTransformer extends TransformerAbstract
{
    /**
     * Transform the Cliente entity.
     *
     * @param \Emplaque\Models\Cliente $model
     *
     * @return array
     */
    public function transform(Cliente $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
