<?php

namespace Emplaque\Transformers;

use League\Fractal\TransformerAbstract;
use Emplaque\Models\Consulta;

/**
 * Class ConsultaTransformer.
 *
 * @package namespace Emplaque\Transformers;
 */
class ConsultaTransformer extends TransformerAbstract
{
    /**
     * Transform the Consulta entity.
     *
     * @param \Emplaque\Models\Consulta $model
     *
     * @return array
     */
    public function transform(Consulta $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'nome' => $model->nome,
            'placa' => $model->placa,
            'renavam' => $model->renavam,
            'email' => $model->email,
            'telefone' => $model->telefone,
            'created_at' => $model->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $model->updated_at->form('Y-m-d H:i:s')
        ];
    }
}
