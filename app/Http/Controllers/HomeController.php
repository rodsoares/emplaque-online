<?php

namespace Emplaque\Http\Controllers;

use Illuminate\Http\Request;

use Emplaque\Services\ConsultaDebitosService;
use Auth;

class HomeController extends Controller
{

    protected $debitosDetranSC;
    
    public function __construct(ConsultaDebitosService $service)
    {
        //$this->middleware('auth');

        $this->debitosDetranSC = $service;
    }

    public function index()
    {
        return view('home');
    }

    public function painel()
    {
        $tipo = Auth::user()->getRoleNames()[0];

        switch( $tipo )
        {
            case "administrador":
                return redirect()->route('admin.dashboard');
                break;

            case "despachante":
                return redirect()->route('despachantes.painel');
                break;

            case "cliente":
                return redirect()->route('clientes.painel');
                break;
        }
    }

    public function consultarDebitos(Request $request)
    {
        $placa = $request->input('placa');
        $renavam = $request->input('renavam');
        $email = $request->input('email');
        $telefone = $request->input('telefone');

        /*
         * Salvando valores em sessão
         */
        $request->session()->put('cliente', [
            'placa' => $placa,
            'renavam' => $renavam,
            'telefone' => $telefone,
            'email' => $email
        ]);

        $debitos = $this->debitosDetranSC->consultar($placa, $renavam);

        $info = $debitos['informacoes_gerais'];
        $debitos = $debitos['debitos'];

        return view('exibir-debitos', [
            'info' => $info,
            'debitos' => $debitos
        ]);
    }
}
