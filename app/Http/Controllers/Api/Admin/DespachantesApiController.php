<?php

namespace Emplaque\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use Emplaque\Http\Controllers\Controller;

use Emplaque\Repositories\Interfaces\DespachanteRepository;

class DespachantesApiController extends Controller
{
    protected $repository;

    public function __construct(DespachanteRepository $repository)
    {
        $this->repository = $repository;
    }    

    public function toDataTables()
    {
        $data = [];
        $data['data'] = $this->repository->all();

        return response()->json($data);
    }

    public function show( $id )
    {
        $despachante = $this->repository->find( $id );
        return response()->json($despachante);
    }
}
