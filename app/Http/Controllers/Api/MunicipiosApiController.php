<?php

namespace Emplaque\Http\Controllers\Api;

use Illuminate\Http\Request;
use Emplaque\Http\Controllers\Controller;
use Emplaque\Repositories\Interfaces\MunicipioRepository;
use Emplaque\Repositories\Interfaces\DespachanteRepository;

class MunicipiosApiController extends Controller
{
    protected $municipioRepo;
    protected $despachanteRepo;

    public function __construct(MunicipioRepository $municipio, DespachanteRepository $despachante)
    {
        $this->municipioRepo = $municipio;
        $this->despachanteRepo = $despachante;
    }

    public function consultar(Request $request, $uf)
    {
        $municipios = $this->municipioRepo->findWhere(['uf' => $uf]);

        if ( $request->has('to-select2') )
        {
            $result = '<option>Selecione um município</option>';

            foreach ($municipios as $municipio )
                $result .= '<option value="'. $municipio->Codigo .'">'. $municipio->Nome .'</option>';

            return response()->json($result);
        } 

        return $municipios;
    }

    /**
     * Monta-se o 'html' para ser inserido no SELECT
     * Warning: Tentei usando a api do proprio plugin, mas ocorria uns erro mei sinistro.
     * TODO: Retirar o 'html' gerado e retornar apenas um objeto json com os dados
     */
    public function despachantes(Request $request, $codigo )
    {
        $despachantes = $this->despachanteRepo->findWhere(
            [
                'status' => 'APROVADO',
                'municipio_codigo' => $codigo
            ]
        );

        if ( $request->has('to-select2') )
        {
            $result = '';

            foreach ($despachantes as $despachante )
                $result .= '<option value="'. $despachante->id .'">'. $despachante->nome .'</option>';

            return response()->json($result);
        } 

        return $despachantes;
    }
}
