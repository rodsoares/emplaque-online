<?php

namespace Emplaque\Http\Controllers\Api;

use Illuminate\Http\Request;
use Emplaque\Http\Controllers\Controller;

use Emplaque\Services\ConsultaDebitosService;

class ConsultaDebitosApiController extends Controller
{
    protected $debitos;

    public function __construct(ConsultaDebitosService $debitos) {
        $this->debitos = $debitos;
    }

    public function index($placa, $renavam)
    {
        $debitos = $this->debitos->consultar( $placa, $renavam );
        return response()->json( $debitos );
    }
}
