<?php

namespace Emplaque\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Emplaque\Http\Controllers\Controller;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

use Emplaque\Repositories\Interfaces\UsuarioRepository;
use Emplaque\Validators\UsuarioValidator;

class UsuariosController extends Controller
{
    protected $repository;
    protected $validator;

    public function __construct(UsuarioRepository $repository, UsuarioValidator $validator) 
    {
        $this->middleware(['role:administrador']);

        $this->repository = $repository;
        $this->validator = $validator;
    }

    public function index(Request $request)
    {
        $usuarios = $this->repository->all();

        if ( $request->has('clientes') ) {
            $aux = [];
            foreach( $usuarios as $usuario ) {
                if( $usuario->hasRole('cliente') ) {
                    $aux[] = $usuario;
                }
            }

            $usuarios = $aux;
        }

        return view('admin.usuarios.index', compact('usuarios'));
    }

    /**
     * Retorna dados via json
     */
    public function show($id)
    {

    }

    public function create()
    {
        return view('admin.usuarios.adicionar');
    }

    public function store(Request $request)
    {
        try {
            $this->validator
                ->with($request->all())
                ->passesOrFail(ValidatorInterface::RULE_CREATE);

            $inputs = $request->all();
            $inputs['password'] = bcrypt( trim($inputs['password']) );

            $usuario = $this->repository->create($inputs);

            /**
             * TODO: Refatorar esse trecho e incluir lá no repositório
             * Acertando tipo do usuario
             */
            $usuario->assignRole($inputs['tipo']);

            $retorno = [
                'mensagem' => "Usuário adicionado com sucesso",
                'data'    => $usuario,
            ];
     
            return redirect()->route('usuarios.index')->with('success', $retorno['mensagem']);
        } catch (ValidatorException $e) {
            return redirect()
                    ->route('usuarios.index')
                    ->withErrors($e->getMessageBag())
                    ->withInput();
        }
    }

    public function edit($id)
    {
        $usuario = $this->repository->find($id);
        return view('admin.usuarios.editar', compact('usuario'));
    }

    public function update(Request $request, $id)
    {
        try {
            $this->validator
                ->with($request->all())
                ->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $inputs = $request->all();

            if ( $request->has('password') ) 
                $inputs['password'] = bcrypt( trim($inputs['password']) );

            if ( $request->has('tipo') ) 
                $usuario = $this->repository->update($inputs, $id);

            /**
             * TODO: Refatorar esse trecho e incluir lá no repositório
             * Acertando tipo do usuario
             */
            $usuario->syncRoles([$inputs['tipo']]);

            $retorno = [
                'mensagem' => "Usuário atualizado com sucesso",
                'data'    => $usuario,
            ];
     
            return redirect()->route('usuarios.index')->with('success', $retorno['mensagem']);
        } catch (ValidatorException $e) {
            return redirect()
                    ->route('usuarios.index')
                    ->withErrors($e->getMessageBag())
                    ->withInput();
        }
    }

    public function destroy($id)
    {
        $usuario = $this->repository->find( $id );
        if ( !$usuario->hasRole('despachante')) {
            $usuario->delete();
            return redirect()->route('usuarios.index')->with('success', 'Usuário deletado da base de dados com sucesso.');
        } else {
            return redirect()->route('usuarios.index')->with('danger', 'Ocorreu algum problema na requisição. Usuário pode está associado à um Despachante.');
        }
    }
}
