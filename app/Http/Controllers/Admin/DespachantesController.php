<?php

namespace Emplaque\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Emplaque\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

use Emplaque\Repositories\Interfaces\DespachanteRepository;
use Emplaque\Validators\DespachanteValidator;

use Emplaque\Repositories\Interfaces\UsuarioRepository;
use Emplaque\Validators\UsuarioValidator;

use Emplaque\Repositories\Interfaces\MunicipioRepository;


class DespachantesController extends Controller
{
    protected $repository;
    protected $validator;
    protected $usuarioRepository;
    protected $municipioRepository;

    public function __construct(
        DespachanteRepository $repository, 
        DespachanteValidator $validator, 
        UsuarioRepository $usuarioRepository, 
        MunicipioRepository $municipioRepository
    )
    {
        $this->middleware(['role:administrador']);

        $this->repository = $repository;
        $this->validator = $validator;
        $this->usuarioRepository = $usuarioRepository;
        $this->municipioRepository = $municipioRepository;
    }

    public function index()
    {
        $despachantes = $this->repository->all();
        return view('admin.despachantes.index', compact('despachantes'));
    }

    public function create()
    {
        $municipios = $this->municipioRepository->findWhere( ['Uf' => 'SC'] );
        return view('admin.despachantes.adicionar', compact('municipios'));
    }

    public function store(Request $request)
    {
        try {
            $this->validator
                ->with($request->all())
                ->passesOrFail(ValidatorInterface::RULE_CREATE);

            $inputs = $request->all();

            /* Salvando e obtendo url da foto salva */
            if ( $request->has('logo') && $request->file('logo')->isValid() ){
                $inputs['logo'] = $this->repository->upload( $request->file('logo') );
            }

            $despachante = $this->repository->create($inputs);

            $retorno = [
                'mensagem' => "Despachante adicionado com sucesso",
                'data'    => $despachante,
            ];
     
            return redirect()->route('despachantes.index')->with('success', $retorno['mensagem']);
        } catch (ValidatorException $e) {
            return redirect()
                    ->route('despachantes.index')
                    ->withErrors($e->getMessageBag())
                    ->withInput();
        }
    }

    public function edit($id)
    {
        $despachante = $this->repository->find( $id );
        $municipios = $this->municipioRepository->findWhere( ['Uf' => 'SC'] );
        return view('admin.despachantes.editar', compact('despachante', 'municipios'));
    }

    public function update(Request $request, $id)   
    {
        try {
            $this->validator
                ->with($request->all())
                ->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $inputs = $request->all();

            $despachante = $this->repository->find( $id );

            /* Salvando/Atualizando e obtendo url da foto salva */
            if ( $request->has('logo') && $request->file('logo')->isValid() ){

                /** apagando arquivo anterior */
                Storage::disk('public')->delete( $despachante->logo );
                
                /** salvando novo arquivo */
                $inputs['logo'] = $this->repository->upload( $request->file('logo') );
            }

            $despachante = $this->repository->update($inputs, $id);

            $retorno = [
                'mensagem' => "Despachante atualizado com sucesso",
                'data'    => $despachante,
            ];
     
            return redirect()->route('despachantes.index')->with('success', $retorno['mensagem']);
        } catch (ValidatorException $e) {
            return redirect()
                    ->route('despachantes.index')
                    ->withErrors($e->getMessageBag())
                    ->withInput();
        }
    }

    public function destroy($id)
    {
        $despachante = $this->repository->find( $id );
        if ( $despachante ) 
        {
            /** apagando arquivo */
            Storage::disk('public')->delete( $despachante->logo );
            
            $despachante->delete();
            return redirect()->route('despachantes.index')->with('success', 'Despachante deletado da base de dados com sucesso.');
        } else {
            return redirect()->route('despachantes.index')->with('danger', 'Ocorreu um erro no processamento da requisição. Tente novamente em alguns instantes.');
        }
    }

    public function enable($id)
    {
        $despachante = $this->repository->find( $id );

        if ( $despachante && !$despachante->user_id )
        {
            $data = [
                'name' => $despachante->nome,
                'email' => trim($despachante->email),
                'password' => bcrypt(trim($despachante->email))
            ];

            try {
                $this->validator
                ->with($data)
                ->passesOrFail(ValidatorInterface::RULE_CREATE);
    
                $usuario = $this->usuarioRepository->create( $data );

                /** associando despachante ao usuário */
                $despachante->user_id = $usuario->id;
                $despachante->status = 'APROVADO';
                
                $usuario->syncRoles(['despachante']);

                $despachante->save();
                $usuario->save();
    
                $retorno = [
                    'mensagem' => "Despachante habilitado com sucesso",
                    'data'    => $despachante,
                ];
         
                return redirect()->route('despachantes.index')->with('success', $retorno['mensagem']);
            } catch (ValidatorException $e) {
                return redirect()
                        ->route('despachantes.index')
                        ->withErrors($e->getMessageBag())
                        ->withInput();
            } 
        } elseif ( $despachante && $despachante->user_id ) {

            $despachante->status = 'APROVADO';
            $despachante->save();
            $retorno = [
                'mensagem' => "Despachante habilitado com sucesso",
                'data'    => $despachante,
            ];
     
            return redirect()->route('despachantes.index')->with('success', $retorno['mensagem']);
        } else {
            return redirect()->route('despachantes.index');
        }
    }

    public function disable($id)
    {
        $despachante = $this->repository->find( $id );

        if ( $despachante )
        {
            $despachante->status = "NEGADO";
            $despachante->save();

            $retorno = [
                'mensagem' => "Despachante desabilitado sucesso",
                'data'    => $despachante,
            ];
     
            return redirect()->route('despachantes.index')->with('success', $retorno['mensagem']);
        } else {
            return redirect()->route('despachantes.index');
        }
    }
}
