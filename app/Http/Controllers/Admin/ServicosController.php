<?php

namespace Emplaque\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Emplaque\Http\Controllers\Controller;

use Emplaque\Models\Servico;

class ServicosController extends Controller
{
    public function index()
    {
        $servicos = Servico::orderBy('id', 'DESC')->paginate(10);
        return view('admin.servicos.index', compact('servicos'));
    }
}
