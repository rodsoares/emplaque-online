<?php

namespace Emplaque\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Emplaque\Http\Controllers\Controller;

use Emplaque\Repositories\Interfaces\ConsultaRepository;

use Emplaque\Models\Despachante;
use Emplaque\Models\User as Usuario;
use Emplaque\Models\Servico;

class DashboardController extends Controller
{
    protected $consultas;

    public function __construct(ConsultaRepository $consulta)
    {
        $this->middleware(['role:administrador']);

        $this->consultas = $consulta;
    }

    public function index()
    {
        $novos_despachantes = count(Despachante::where('status', 'AGUARDANDO APROVACAO')->get());
        $total_despachantes = count(Despachante::all());
        $total_clientes = 0;
        foreach( Usuario::get() as $usuario ) {
            if( $usuario->hasRole('cliente') ) {
                $total_clientes++;
            }
        }
        $total_servicos_em_aberto = count(Servico::whereIn('status', ['APROVADO', 'INICIADO', 'AGUARDANDO PROCESSAMENTO', 'AGUARDANDO COPIA DE DOCUMENTOS'])->get());
        $total_servicos_realizados = count(Servico::where('status', 'FINALIZADO')->get());
        return view('admin.dashboard', 
            compact(
                'novos_despachantes',
                'total_despachantes',
                'total_clientes',
                'total_servicos_em_aberto',
                'total_servicos_realizados'
            )
        );
    }
}
