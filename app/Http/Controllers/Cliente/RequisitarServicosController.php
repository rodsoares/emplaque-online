<?php

namespace Emplaque\Http\Controllers\Cliente;

use Illuminate\Http\Request;
use Emplaque\Http\Controllers\Controller;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

use Emplaque\Repositories\Interfaces\ServicoRepository;
use Emplaque\Validators\ServicoValidator;

class RequisitarServicosController extends Controller
{
    protected $repository;

    public function __construct(ServicoRepository $repository, ServicoValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    public function store(Request $request)
    {
        try {
            $this->validator
                ->with($request->all())
                ->passesOrFail(ValidatorInterface::RULE_CREATE);

            $inputs = $request->all();

            $servico = $this->repository->create($inputs);

            $retorno = [
                'mensagem' => "Serviço requisitado. Aguardo processamento pelo despachante escolhido. Caso não tenha feito o pagamento de todas as taxas, faça o upload do seu RG e CPF",
                'data'    => $servico,
            ];
     
            /** 
             * TODO: Enviar email alertando despachante escolhido sobre o serviço  
             */

            return redirect()->route('clientes.painel')->with('warning', $retorno['mensagem']);
        } catch (ValidatorException $e) {
            return redirect()
                    ->route('clientes.painel')
                    ->withErrors($e->getMessageBag())
                    ->withInput();
        }
    }
}
