<?php

namespace Emplaque\Http\Controllers\Cliente;

use Auth;

use Illuminate\Http\Request;
use Emplaque\Http\Controllers\Controller;

use Emplaque\Services\ConsultaDebitosService;


class ConsultarDebitosController extends Controller
{
    protected $debitosDetranSC;

    public function __construct(ConsultaDebitosService $debitosService)
    {
        $this->debitosDetranSC = $debitosService;
    }

    public function consultar(Request $request)
    {
        $placa = $request->input('placa');
        $renavam = $request->input('renavam');

        /*
         * Salvando valores em sessão
         */
        $request->session()->put('cliente', [
            'placa' => $placa,
            'renavam' => $renavam,
            'email' => Auth::user()->email,
            'telefone' => Auth::user()->telefone
        ]);

        $debitos = $this->debitosDetranSC->consultar($placa, $renavam);

        $info = $debitos['informacoes_gerais'];
        $debitos = $debitos['debitos'];

        return view('cliente.exibir-debitos', [
            'info' => $info,
            'debitos' => $debitos
        ]);
    }
}
