<?php

namespace Emplaque\Http\Controllers\Cliente;

use Illuminate\Http\Request;
use Emplaque\Http\Controllers\Controller;

use Auth;

use Emplaque\Repositories\Interfaces\DespachanteRepository;
use Emplaque\Repositories\Interfaces\ServicoRepository;


class PainelController extends Controller
{

    protected $despachanteRepo;
    protected $servicoRepo;

    public function __construct(
        DespachanteRepository $despachanteRepo, 
        ServicoRepository $servicoRepo   
    )
    {
        $this->middleware(['role:cliente']);

        $this->despachanteRepo = $despachanteRepo;
        $this->servicoRepo = $servicoRepo;
    }

    public function index()
    {
        $despachantes = $this->despachanteRepo->findWhere(['status' => 'APROVADO']);
        $servicos = $this->servicoRepo->findWhere([
                'user_id' => Auth::user()->id,
                ['status', '!=', 'FINALIZADO' ],
                ['status', '!=', 'NEGADO']
            ]);
        $cidades  = [];
        return view('cliente.painel', compact('despachantes', 'servicos', 'cidades'));
    }
}
