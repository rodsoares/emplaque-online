<?php

namespace Emplaque\Http\Controllers\Cliente;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Emplaque\Http\Controllers\Controller;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

use Emplaque\Repositories\Interfaces\ArquivoRepository;
use Emplaque\Validators\ArquivoValidator;

use Auth;


class AnexarArquivosController extends Controller
{
    protected $arquivoRepo;
    protected $validator;

    public function __construct(ArquivoRepository $arquivoRepo, ArquivoValidator $validator)
    {
        $this->arquivoRepo = $arquivoRepo;
        $this->validator = $validator;
    }


    public function store(Request $request)
    {
        try {
            $this->validator
                ->with($request->all())
                ->passesOrFail(ValidatorInterface::RULE_CREATE);

            $inputs = $request->all();

            /* Salvando e obtendo url da foto salva */
            if ( $request->has('documento') && $request->file('documento')->isValid() ){
                $inputs['caminho'] = $this->arquivoRepo->upload( $request->file('documento') );
            }

            $arquivo = $this->arquivoRepo->create($inputs);

            $retorno = [
                'mensagem' => "Arquivo anexado com sucesso",
                'data'    => $arquivo,
            ];
     
            $rota = null;
            if ( Auth::user()->hasRole('cliente'))
            {
                $rota = 'clientes.painel';
            } else {
                $rota = 'despachantes.painel';
            }

            /** 
             * TODO: Enviar email alertando despachante escolhido sobre o documento upado  
             */

            return redirect()->route($rota)->with('warning', $retorno['mensagem']);
        } catch (ValidatorException $e) {
            return redirect()
                    ->route($rota)
                    ->withErrors($e->getMessageBag())
                    ->withInput();
        }
    }

    public function download( $id ) 
    {
        $documento = $this->arquivoRepo->find( $id );

        if ( $documento )
        {
            return Storage::disk('public')->download($documento->caminho);
        } else {
            return response(404);
        }
    }
}
