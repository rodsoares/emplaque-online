<?php

namespace Emplaque\Http\Controllers\Despachante;

use Auth;

use Illuminate\Http\Request;
use Emplaque\Http\Controllers\Controller;

use Emplaque\Repositories\Interfaces\ServicoRepository;

class PainelController extends Controller
{
    protected $servicoRepo;

    public function __construct(ServicoRepository $servicoRepo)
    {
        $this->middleware(['role:despachante']);

        $this->servicoRepo = $servicoRepo;
    }

    public function index()
    {
        $servicos = $this->servicoRepo->findWhere([
                        'despachante_id' => Auth::user()->despachante->id,
                        ['status', '!=', 'FINALIZADO' ]
                    ]);
        return view('despachante.painel', compact('servicos'));
    }
}
