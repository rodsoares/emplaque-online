<?php

namespace Emplaque\Http\Controllers\Despachante;

use Illuminate\Http\Request;
use Emplaque\Http\Controllers\Controller;
use Emplaque\Repositories\Interfaces\ServicoRepository;

class ServicosController extends Controller
{
    protected $servicoRepo;

    public function __construct(ServicoRepository $servicoRepo)
    {
        $this->servicoRepo = $servicoRepo;
    }

    public function alterarStatus(Request $request)
    {
        $servico = $this->servicoRepo->find( $request->input('servico_id') );
        if ( $servico )
        {
            $servico->status = $request->input('status');
            $servico->save();
            $retorno = [
                'mensagem' => "Situação do serviço alterada com sucesso. Um email foi enviado alertando o cliente dessa alteração.",
                'data'    => $servico,
            ];
     
            /** 
             * TODO: Enviar email alertando cliente sobre alteração do status  
             */

            return redirect()->route('despachantes.painel')->with('success', $retorno['mensagem']);
        } else {
            return response( 404 );
        }
    }
}
